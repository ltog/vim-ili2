" Vim INTERLIS 2 syntax file
" Author: Lukas Toggenburger, https://github.com/ltog/vim-ili2
" License: Unlicense, see http://unlicense.org/

if exists("b:current_syntax")
	finish
endif

" INTERLIS 2 keywords (will be highlighted as 'Keyword')
syn keyword commonKeyword ABSTRACT ACCORDING AGGREGATES AGGREGATION ALL AND ANY ANYCLASS ANYSTRUCTURE AS AT ATTRIBUTE BAG BASE BASED BASKET BINARY BLACKBOX BOOLEAN BY CARDINALITY CIRCULAR CLOCKWISE CONSTRAINTS CONTINUOUS CONTRACTED COORD COUNTERCLOCKWISE DEFINED DEPENDS DERIVED DOMAIN ENUMTREEVAL ENUMVAL EQUAL EXTENDED EXTERNAL FIRST FORM FORMAT FROM FUNCTION GRAPHIC HALIGNMENT HIDING IMPORTS IN INHERITANCE INSPECTION INTERLIS JOIN LAST LINE LIST LOCAL METAOBJECT MTEXT NAME NO NOT NULL NUMERIC OBJECT OBJECTS OF OID ON OR OTHERS OVERLAPS PARAMETER PARENT PROJECTION REFERENCE REFSYSTEM REQUIRED RESTRICTION ROTATION SIGN SUBDIVISION SYMBOLOGY THATAREA THIS THISAREA TO TRANSIENT TRANSLATION TYPE UNDEFINED UNION UNIT UNQUALIFIED URI VALIGNMENT VERSION VIEW WHEN WHERE WITH WITHOUT

" INTERLIS 2 keywords (will be highlighted as keywords; the following word will be highlighted as 'Identifier')
syn keyword keywordBeforeName ASSOCIATION ATTRIBUTES CLASS END EXTENDS MODEL STRUCTURE TOPIC VERTEX nextgroup=nameRE skipwhite

" content after !! will be marked as 'Comment'; the comments may contain todos
syn match comment "!!.*$" contains=todo

" keywords that only exist in INTERLIS 1 will be marked as 'Error'
syn keyword ili1Keyword BLANK CODE CONTINUE CONTOUR COORD2 COORD3 DATE DEFAULT DEGREES DERIVATIVES DIM1 DIM2 FIX FONT FREE GRADS I16 I32 IDENT LINEATTR LINESIZE OPTIONAL PERIPHERY RADIANS TABLE TID TIDSIZE TRANSFER VERTEXINFO

" keywords that describe 'Type's
syn keyword types AREA ARCS POLYLINE STRAIGHTS SURFACE

" mark these as 'Constant'
syn keyword constants LNBASE PI

syn keyword operators ..

syn match textTypeRE 'TEXT\*[0-9]*'

syn match attributeName '[A-Za-z0-9_]\{1,256\}:'
" may not work always, see INTERLIS 2 reference manual, section 2.6.1, syntax rule for AttributeDef

syn match nameRE '[A-Za-z0-9_]\{1,256\}' contained " 'contained' means: only recognize this pattern if it occurs in another pattern that has a reference pointing here, see also https://stackoverflow.com/questions/1819006/vim-syntax-files-trying-to-undestand-contains

syn match intNumber '\s-\?[0-9]\+\s' " match 123 and -123 if it has a whitespace before and after

syn match floatNumber '\s-\?[0-9]\+\.[0-9]\+\s' " match 123.45 and -123.45 if it has a whitespace before and after

syn match string '"[^"]*"'

syn keyword constraint CONSTRAINT DIRECTED EXISTENCE FINAL MANDATORY ORDERED SET UNIQUE

syn keyword todo contained TODO todo FIXME fixme XXX xxx NOTE note

let b:current_syntax = "ili2"

hi def link commonKeyword     Keyword
hi def link keywordBeforeName Keyword
hi def link comment           Comment
hi def link ili1Keyword       Error
hi def link types             Type
hi def link structures        Structure
hi def link constants         Constant
hi def link operators         Operator
hi def link textTypeRE        Type
hi def link attributeName     Identifier
hi def link nameRE            Identifier
hi def link intNumber         Number
hi def link floatNumber       Float
hi def link string            String
hi def link constraint        StorageClass
