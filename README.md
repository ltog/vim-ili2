vim-ili2
========

Vim syntax highlighting for INTERLIS 2 model files.

Created while working on https://github.com/ltog/xtfvt .

Screenshot
----------

![Screenshot](https://raw.githubusercontent.com/ltog/vim-ili2/master/screenshot.png)

State of development
--------------------

No further development is planned at the moment. Pull requests are welcome.

License
-------

Unlicense (see http://unlicense.org/ )

Contact
-------

Lukas Toggenburger ( lukas.toXXenburger@htwchur.ch , replace XX with gg)
