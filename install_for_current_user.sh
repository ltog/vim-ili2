#!/bin/bash

# Author: Lukas Toggenburger, https://github.com/ltog/vim-ili2
# License: Unlicense, see http://unlicense.org/

mkdir -p ~/.vim/syntax
cp ./ili2.vim ~/.vim/syntax/

mkdir -p ~/.vim/ftdetect
echo 'au BufRead,BufNewFile *.ili setfiletype ili2' > ~/.vim/ftdetect/ili2.vim
